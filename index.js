/*console.log("Siesta time!");*/



/*function example(parameter){
	// console.log(parameter);

	return "Hi I am the returned value from the function."
}


console.log(typeof example("Hi I am the argument!"));

let secondExample = example("Hi I am the argument!");
console.log(secondExample);*/




// ARRAY METHODS
	//JavaScript has built-in functions and methods for arrays. This allows us to manipulate and access array elements.


// [SECTION] Mutator Methods
	// Mutator methods are functions that mutate or change an array they've created.
	// these methods manipulate the original array performing various tasks as adding and removing elements.

	let fruits = ['Apple','Orange', 'Kiwi', 'Dragon Fruit'];

console.log(fruits);
	// First Mutator Method: push()
		// adds element/s in the end of an array and "RETURNS THE ARRAY'S LENGTH".
		// SYNTAX:
		// arrayName.push(elementToBeAdded);

console.log("Current Array fruits []:");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log("Mutated Array after the push method:");
console.log(fruits);
console.log(fruitsLength);

// Adding multiple elements to an array
fruitsLength = fruits.push("Avocado","Guava");
console.log("Mutated Array from the push method:");
console.log(fruits);
console.log(fruitsLength);

	// Second Mutator Method: pop()
		// removes the last element in an array and "RETURNS THE REMOVED ELEMENT".
		// SYNTAX:
			// arrayName.pop();

console.log("Current Array fruits []:");
console.log(fruits);

let removedFruit = fruits.pop();
console.log("Mutated Array after the pop method:");
console.log(fruits);
console.log(removedFruit);

	// Third Mutator Method: unshift()
		// it adds one or more elements at the beginning of an array and "RETURNS THE PRESENT LENGTH".
		// SYNTAX:
			// arrayName.unshift(elementToBeAdded);

console.log("Current Array fruits []:");
console.log(fruits);

fruitsLength = fruits.unshift("Lime","Banana");
console.log("Mutated Array after the unshift method:");
console.log(fruits);
console.log(fruitsLength);

	// Fourth Mutator Method: shift()
		// remove an element at the beginning of an array and "RETURNS THE REMOVED ELEMENT".
		// SYNTAX:
			// arrayName.shift();

console.log("Current Array fruits []:");
console.log(fruits);

removedFruit = fruits.shift();
console.log("Mutated Array after the shift method:");
console.log(fruits);
console.log(removedFruit);

	// Fifth Mutator Method: splice()
		// "SIMULTANEOUSLY REMOVES" elements from a "specified index" number "AND ADDS" elements.
		// SYNTAX:
			// arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

console.log("Current Array fruits []:");
console.log(fruits);

let splice = fruits.splice(1,2,"Lime", "Guava");
console.log("Mutated Array after the splice method:");
console.log(fruits);
console.log(splice); // deletes two then add 2 from startingIndex

fruits.splice(1, 2);
console.log(fruits); //only deletes, no replacement

console.log("Current Array fruits []:");
console.log(fruits);
fruits.splice(2, 0, "Durian", "Santol");
console.log(fruits);

	// Sixth Mutator Method: sort()
		// rearrange the array element on alphanumeric order
		// SYNTAX:
			// arrayName.sort();

console.log("Current Array fruits []:");
console.log(fruits[0]);
console.log(fruits);

fruits.sort();
console.log("Mutated Array after the sort method:");
console.log(fruits[0]);
console.log(fruits);

	// Seventh Mutator Method: reverse()
		// reverses the order of array elements
		// SYNTAX:
			// arrayName.reverse();

console.log("Current Array fruits []:");
console.log(fruits[0]);
console.log(fruits);

fruits.reverse();
console.log("Mutated Array after the sort method:");
console.log(fruits[0]);
console.log(fruits);

// [SECTION] Non-Mutator Methods

	// Non-Mutator methods are functions that do not modify or change the array after they're created.
	// These methods do not manipulate the original array performing task such as returning elements from an array and combining arrays and printing the output.

let countries = ["US","PH","CAN","SG","TH","PH","FR","DE"];

	// First Non-Mutator Method: indexOf()
		// it "RETURNS THE INDEX NUMBER" of the "FIRST" matching element found in an array.
		// if no match was found the result will be -1.
		//  the search process will be done from first element proceeding to the last element.
		// SYNTAX:
			// arrayName.indexOf(searchValue);

console.log(countries);
console.log(countries.indexOf('PH')); //1, displays index
console.log(countries.indexOf('BR')); // -1, no match
		
		// in indexOf() we can set the starting index

console.log(countries.indexOf('PH',2));

	// Second Non-Mutator Method: lastIndexOf()
		// it "RETURNS THE INDEX NUMBER" of the "LAST" matching element found in an array.
		
		// SYNTAX:
			// arrayName.lastIndexOf(searchValue);

console.log(countries.lastIndexOf('PH',2));

	// Third Non-Mutator Method: splice()
		// portion/slices from array and "RETURNS A NEW ARRAY"
		
		// SYNTAX:
			// arrayName.splice(startingIndex);
			// arrayName.splice(startingIndex, endingIndex);

let slicedArrayA = countries.slice(2);
console.log(slicedArrayA); //displays sliced array from starting index
console.log(countries); //displays non-mutated/original array

let slicedArrayB = countries.slice(1,5);
console.log(slicedArrayB); //displays sliced array from starting index (1) to before ending index (5), index5 not included.

	// Fourth Non-Mutator Method: toString()
		// return an array as string separated by comma
		
		// SYNTAX:
			// arrayName.toString();

let stringedArray = countries.toString();
console.log(stringedArray); //no spaces included
console.log(stringedArray.length); //checks length


	// Fifth non-mutator method: concat()
		// combines arrays and returns the combined result
		
		// SYNTAX:
			// arrayA.concat(elementA)

let tasksArrayA = ["drink HTML", "eat javascript"];
let tasksArrayB = ['inhale CSS', 'breathe SASS'];
let tasksArrayC = [ 'get git', 'be node'];

let tasks = tasksArrayB.concat(tasksArrayA);
console.log(tasks);

//adding/combining multiple arrays
let allTasks = tasksArrayA.concat(tasksArrayC, tasksArrayB);
console.log(allTasks);

//combine arrays with elements
let combinedTasks = tasksArrayA.concat(tasksArrayB, "smell express", 'throw react');
console.log(combinedTasks)


	// Sixth Non-Mutator Method: join()
		// returns an array as string separated by a specified separator string.
		
		// SYNTAX:
			// arrayName.join(separatorString);

let users = ["John","Jane","Joe","Robert"];

console.log(users.join()); //no separator, default comma
console.log(users.join(" ")); // w/separator, whitespace
console.log(users.join("-"));


// [SECTION] Iteration Methods
	// iteration methods are loop designed to perform, repetitive tasks on arrays
	// iteration method loop over all elements in an array.

	// First Iteration Method: forEach()
		// similar to FOR Loop that iterates on each array element
		// for each element in the array, the function in the forEach Method will run.
		// DOESNT RETURN ANY VALUE
	/*
		SYNTAX:
			arrayName.forEach(function(indivElement){
				statement/statment;
			}
	*/

console.log(allTasks);

let filteredTask = [];
allTasks.forEach(function(task){
	if(task.length > 10){

		filteredTask.push(task);

	}
})
console.log(filteredTask);


	// SecondIteration Method: map()
		// iterates on each element and "RETURNS NEW ARRAY" with different values depending on the result of the function's operation.
	/*
		SYNTAX:
			arrayName.map(function(element){
				statements;
				return;
			}
	*/

let numbers = [1, 2, 3, 4, 5];
console.log(numbers);

let numberMap = numbers.map(function(number){

	return number*number; //return squared value of the elements
})
console.log(numberMap);

	// Third Iteration Method: every()
		// checks if all elements in an array meet the given condition.
		// this is useful in validationg data stored in arrays especially when dealing with large amounts of data.
		// return TRUE value if all elements meet the condition and false if otherwise.
	/*
		SYNTAX:
			arrayName.every(function(element){
				return expression/condition
			})
	*/

console.log(numbers);
// let allInvalid = [];
let allValid = numbers.every(function(number){
	// return (number<5); // false, if at least one fails to meet condition
	return (number<6); //returns true, if all is true
	// if(number<5){

	// }else{
	// 	allInvalid.push(number);
	// }
})
console.log(allValid);


	// Fourth Iteration Method: some()
		// checks if alt least one element in the array meets the given condition.
		// return TRUE value if at least one element meets the condition and false if none.
	/*
		SYNTAX:
			arrayName.some(function(element){
				return expression/condition
			})
	*/

console.log(numbers);

let someValid = numbers.some(function(number){
	return (number<5); //true, if at least one meets condition
	// return (number<0); //returns false if all is false
})


	// Fifth Iteration Method: filter()
		// "RETURNS A NEW ARRAY" that contains the element which meets the given condition
		// return EMPTY ARRAY if no element were found
	/*
		SYNTAX:
			arrayName.filter(function(element){
				return expression/condition
			})
	*/

console.log(numbers);
let filterValid = numbers.filter(function(number){
	return (number <= 3);
})
console.log(filterValid);


	// Sixth Iteration Method: includes()
		// checks if the argument passed can be found in the array.
		// it returns boolean which can be saved in a variable.
			// retrun TRUE if the argument is found in the array.
			// return false if not found.
	/*
		SYNTAX:
			arrayName.includes()
	*/

let products = ["mouse","keyboard","laptop","monitor"];
console.log(products);

let productFound1 = products.includes("mouse");
console.log(productFound1);

let productFound2 = products.includes("Mouse");
console.log(productFound2);


	// Seventh Iteration Method: reduce()
		// evaluate elements from left to right and "RETURNS THE REDUCED ARRAY"
		// array will trun into single value
	/*
		SYNTAX:
			arrayName.reduce(function(accumulator, currentValue){
				return operation/expression;
			})
	*/

console.log(numbers);

let total = numbers.reduce(function(x, y){
	console.log("This is the value of x: "+x);
	console.log("This is the value of y: "+y);
	return x + y;
})

console.log(total);